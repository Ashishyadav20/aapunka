(function () {
  'use strict';
  var appSetting = {
    'baseUrl': 'https://apunka.herokuapp.com/v1/',
    'baseImageUrl': 'https://apunka.herokuapp.com/images/'
  }
  angular
    .module('angularProjects')
    .constant('appSetting', appSetting);
})();

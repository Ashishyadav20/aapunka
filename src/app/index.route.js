(function () {
  'use strict';

  angular
    .module('angularProjects')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
        url: '/',
        templateUrl: 'app/login/login.html',
        controller: 'loginController',
        controllerAs: 'vm'
      })
      .state('home', {
        url: '/home',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'vm'
      })
      .state('product', {
        url: '/product/:type/:id',
        templateUrl: 'app/productList/productList.html',
        controller: 'ProductListController',
        controllerAs: 'vm',
        resolve: {
          sample: function (AppService) {
            console.log("Appservice", AppService);
            return { "name": "Ashish yadav", 'age': 24 };
          },
          sample1: function () {
            return { "name": "God Devil yadav", 'age': 42 };
          }
        }
      })
      .state('productDetails', {
        url: '/productDetails/:id/:name',
        templateUrl: 'app/productDetails/productDetails.html',
        controller: 'ProductDetailsController',
        controllerAs: 'vm'
      })
      .state('orderPlaced', {
        url: '/orderPlaced/:orderId',
        templateUrl: 'app/orderPlaced/orderPlaced.html',
        controller: 'OrderPlacedController',
        controllerAs: 'vm',
        resolve: {
          orderResponse: ['$stateParams', 'AppService', '$state', function ($stateParams, AppService, $state) {
            var userId = AppService.AppFactory.getUserCookie();
            var orderId = $stateParams.orderId;
            return AppService.getOrderDetails(userId, orderId);
          }]
        }
      })
      .state('billingConfirmation', {
        url: '/confirmation',
        templateUrl: 'app/confirmation/confirmation.html',
        controller: 'ConfirmationController',
        controllerAs: 'vm'
      })
      .state('shoppingCart', {
        url: '/shoppingCart',
        templateUrl: 'app/shoppingCart/shoppingCart.html',
        controller: 'ShoppingCartController',
        controllerAs: 'vm'
      });

    $urlRouterProvider.otherwise('/');
  }

})();

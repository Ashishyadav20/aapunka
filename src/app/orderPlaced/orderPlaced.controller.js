(function () {
  'use strict';

  angular
    .module('angularProjects')
    .controller('OrderPlacedController', OrderPlacedController);

  /** @ngInject */
  function OrderPlacedController($rootScope, AppService, $state, $stateParams, orderResponse) {
    var vm = this;
    if ('data' in orderResponse && 'result' in orderResponse.data) {
      if ('success' in orderResponse.data && orderResponse.data.success) {
        vm.orderDetails = orderResponse.data;
        console.log("vm.orderDetails", vm.orderDetails);
      }

    }
    console.log("activate is called");
    activate();
    function activate() {
      vm.colorSetting = {
        'background': '#000',
        'color': '#fff'
      }
      vm.quantity = 1;
      vm.todayDate = new Date();
      vm.shippingCharge = 20;
      vm.cartsList = AppService.carts;
      vm.getPrice = getPrice;
      vm.removeitem = removeitemFromCart;
      vm.quantityChange = updateChanges;
      getSubTotalPrice();
      getTotalPrice();
      vm.keppShoping = keppShoping;
      vm.isContrast = false;
    }

    function updateChanges() {
      getSubTotalPrice();
      getTotalPrice();
    }
    function keppShoping() {
      $state.go('home', {}, { location: 'replace' });

    }
    function getTotalPrice() {
      vm.total = getSubTotalPrice();
      vm.total = vm.total + vm.shippingCharge;
    }
    function getSubTotalPrice() {
      vm.subTotal = 0;
      angular.forEach(vm.cartsList, function (element) {
        var price = parseInt(element.Price);
        price = price * parseInt(element.quantity);
        vm.subTotal = vm.subTotal + price;
      }, this);
      return vm.subTotal;
    }
    function getPrice(price, units) {
      if (units > 0) {
        return price * units;
      } else {
        return price;
      }

    }
    function removeitemFromCart(item) {
      AppService.removeItemFromCart(item);
      vm.cartsList = AppService.carts;
    }


    $rootScope.$on('onSearch', function ($event, data) {
      vm.searchText = data;
    })
  }
})();

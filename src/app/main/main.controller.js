(function () {
  'use strict';

  angular
    .module('angularProjects')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($rootScope, $state, AppService) {
    var vm = this;
    vm.appSetting = AppService.appSetting;
    activate();
    function activate() {
      vm.colorSetting = {
        'background': '#000',
        'color': '#fff'
      };
      $rootScope.$on('isContrast', function (ev, data) {
        vm.isContrast = data;
      });
      AppService.AppFactory.getUserCookie();
      vm.dataFoundStatus = true;
      vm.isContrast = JSON.parse(AppService.getLocalStorageItem('contrast')) != null ? JSON.parse(AppService.getLocalStorageItem('contrast')) : false;
      vm.openProduct = openProduct;
      getAllProducts();
    }

    function getAllProducts() {
      AppService.getAllProducts().then(function (success) {
        console.log("Products success", angular.toJson(success));
        if (success.data.hasOwnProperty('result')) {
          vm.dashList = success.data.result;
        } else {
          vm.dataFoundStatus = false;
        }

      }, function (error) {
        console.log("Products error", angular.toJson(error));
      });
    }


    function openProduct(item) {
      $state.go('product', {
        id: item.product_id, type: item.category //selectedItem and id is defined
      });
    }

    function getDashboardList() {
      var jsonList = [{
        'id': 1,
        product_name: 'Canes',
        image_url: 'assets/images/canes.jpg'
      }, {
        'id': 2,
        product_name: 'Voice Recorders  ',
        image_url: 'assets/images/voice_recorders.jpg'
      }, {
        'id': 3,
        product_name: 'Screen Readers',
        image_url: 'assets/images/screen_readers.jpg'
      }, {
        'id': 4,
        product_name: 'Braille Devices',
        image_url: 'assets/images/braille.jpg'
      }, {
        'id': 5,
        product_name: 'Audio Books',
        image_url: 'assets/images/audio_books.jpg'
      }, {
        'id': 6,
        product_name: 'Games & Sports',
        image_url: 'assets/images/games_and_sports.jpg'
      }, {
        'id': 7,
        product_name: 'Watches & Clocks',
        image_url: 'assets/images/watches.jpg'
      }, {
        'id': 8,
        product_name: 'Learning tools',
        image_url: 'assets/images/tools.jpg'
      }, {
        'id': 9,
        product_name: 'Typing Aids',
        image_url: 'assets/images/typings.jpg'
      }];
      return jsonList;
    }
    $rootScope.$on('onSearch', function ($event, data) {
      vm.searchtext = data;
    });
  }
})();

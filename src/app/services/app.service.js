(function () {
    'use strict';

    angular
        .module('angularProjects')
        .service('AppService', AppService);

    /** @ngInject */
    function AppService($window, $q, appSetting, $http, appFactory) {
        return {
            "getAllProducts": getAllProducts,
            "getProductDetailsByID": getProductDetailsByID,
            "setLocalStorageItem": setLocalStorageItem,
            "clearAllLocalStorage": clearAllLocalStorage,
            "removeLocalStorageItem": removeLocalStorageItem,
            "getLocalStorageItem": getLocalStorageItem,
            "setSelectedProduct": setSelectedProduct,
            "getSelectedProduct": getSelectedProduct,
            "appSetting": appSetting,
            "AppFactory": appFactory,
            "addItemToCarts": addItemToCarts,
            "getCartItems": getCartItems,
            "removeItemFromCart": removeItemFromCart,
            "placeOrder": placeOrder,
            "getOrderDetails": getOrderDetails

        };
        function setSelectedProduct(product) {
            $window.localStorage.setItem('selectedProduct', JSON.stringify(product));
        }
        function getSelectedProduct() {
            var selectedProduct = $window.localStorage.getItem('selectedProduct');
            return JSON.parse(selectedProduct);
        }
        function addItemToCarts(item) {
            console.log("Add items", angular.toJson(item));
            return servicePostCall(setPostRequest('cart', item));
        }

        function getCartItems(id) {
            return servicePostCall(setRequest('cart?id=' + id, 0));
        }

        function placeOrder(jsonRequest) {
            //  https://apunka.herokuapp.com/v1/order
            var jsonInput = setPostRequest('order', jsonRequest);
            return servicePostCall(jsonInput);

        }

        function removeItemFromCart(quest_id, catlog_id) {
            // https://apunka.herokuapp.com/v1/cart?guest_id=12345678901234&catalog=Vr002
            var jsonInput = setRequest('cart?guest_id=' + quest_id + '&catalog=' + catlog_id, 1);
            return serviceGetCall(jsonInput);
        }

        function getOrderDetails(orderId, userId) {
            // https://apunka.herokuapp.com/v1/order?id=12345678901234&order_id=1514914582046
            var jsonInput = setRequest('order?id=' + userId + '&order_id=' + orderId, 0);
            return serviceGetCall(jsonInput);
        }

        function setLocalStorageItem(key, dataInput) {
            $window.localStorage.setItem(key, dataInput);
        }
        function clearAllLocalStorage() {
            $window.localStorage.clear();
        }
        function removeLocalStorageItem(key) {
            $window.localStorage.removeItem(key);
        }

        function getLocalStorageItem(key) {
            return $window.localStorage.getItem(key);
        }

        function getAllProducts() {
            var jsonInput = setRequest('products', 0);
            return serviceGetCall(jsonInput);
        }

        function getProductDetailsByID(id) {
            var jsonInput = setRequest('products?id=' + id, 0);
            return serviceGetCall(jsonInput);
        }

        function serviceGetCall(jsonInput) {
            var defer = $q.defer();
            var param = {
                method: jsonInput.method,
                url: appSetting.baseUrl + jsonInput.service
            }
            $http(param).then(function (success) {
                defer.resolve(success);
            }, function (error) {
                defer.reject(error);
            });
            return defer.promise;
        }

        function servicePostCall(jsonInput) {
            var defer = $q.defer();
            var param = {
                method: jsonInput.method,
                url: appSetting.baseUrl + jsonInput.service,
                headers: { "Content-Type": "application/json" },
                data: jsonInput.data
            }
            $http(param).then(function (success) {
                defer.resolve(success);
            }, function (error) {
                defer.reject(error);
            });
            return defer.promise;
        }

        function setPostRequest(service, dataRequest) {
            var jsonRequest = {
                "method": "POST",
                "data": dataRequest,
                "service": service
            }
            return jsonRequest;
        }

        function setRequest(service, type) {
            /**
             * type 0 =="GET"
             * type 1 =="DELETE"
             */
            var jsonInput = {
                "service": service,
                "method": type == 0 ? 'GET' : 'DELETE'
            }
            return jsonInput;
        }
    }
})();

(function () {
    'use strict';
    angular.module('angularProjects')
        .filter('cityList', cityList);
    function cityList() {
        return function (lists, list) {
            var cityListResponse = [];
            if(lists){
                for (var pos = 0; pos < lists.length - 1; pos++) {
                var element = lists[pos];
                if (element.state === list) {
                    cityListResponse.push(element);
                }
            }
            }
            
            return cityListResponse;

        }
    }
})();
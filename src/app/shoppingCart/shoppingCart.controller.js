(function () {
  'use strict';

  angular
    .module('angularProjects')
    .controller('ShoppingCartController', ShoppingCartController);

  /** @ngInject */
  function ShoppingCartController($rootScope, AppService, $state) {
    var vm = this;
    activate();
    function activate() {
      vm.appSetting = AppService.appSetting;
      vm.colorSetting = {
        'background': '#000',
        'color': '#fff'
      }
      $rootScope.$on('isContrast', function (ev, data) {
        vm.isContrast = data;
      });
      vm.isContrast = JSON.parse(AppService.getLocalStorageItem('contrast')) != null ? JSON.parse(AppService.getLocalStorageItem('contrast')) : false;

      vm.quantity = 1;
      vm.shippingCharge = 20;
      getCarts();
      vm.getPrice = getPrice;
      vm.removeitem = removeitemFromCart;
      vm.quantityChange = updateChanges;

      vm.keppShoping = keppShoping;
      vm.checkout = placeOrder;
    }

    function getCarts() {
      AppService.getCartItems(AppService.AppFactory.getUserCookie()).then(function (success) {
        if (success.hasOwnProperty('data')) {
          if (success.hasOwnProperty('data')) {
            vm.cartsList = success.data.result;
            getSubTotalPrice(function (response) {
              getTotalPrice(response);
            });;
          }

        }
      }, function (error) {
      })
    }

    function updateChanges() {
      getSubTotalPrice(function (response) {
        getTotalPrice(response);
      });

    }
    function placeOrder() {
      $state.go('billingConfirmation');
    }
    function keppShoping() {
      $state.go('home', {}, { location: 'replace' });

    }
    function getTotalPrice(response) {
      vm.total = response + vm.shippingCharge;
      // getSubTotalPrice(function (response) {
      //   vm.total = response + vm.shippingCharge;
      // });
    }
    function getSubTotalPrice(callback) {
      vm.subTotal = 0;
      angular.forEach(vm.cartsList, function (element) {
        var price = parseInt(element.cost);
        price = price * parseInt(element.qty != undefined ? element.qty : '1');
        vm.subTotal = vm.subTotal + price;
      }, this);
      callback(vm.subTotal);
    }
    function getPrice(price, units) {
      if (units > 0) {
        return price * units;
      } else {
        return price;
      }

    }
    function removeitemFromCart(item) {
      console.log("remove item", item);
      AppService.removeItemFromCart(AppService.AppFactory.getUserCookie(), item.catalog_id).then(function (success) {
        console.log(JSON.stringify(success));
        if (success.hasOwnProperty('data')) {
          if (success.data.hasOwnProperty('success')) {
            if (success.data.success) {
              $state.go($state.$current, null, { reload: true });
            }

          }

        }
      });
      vm.cartsList = AppService.carts;
      getSubTotalPrice(function (response) {
        getTotalPrice(response);
      });

    }


    $rootScope.$on('onSearch', function ($event, data) {
      vm.searchText = data;
    })
  }
})();

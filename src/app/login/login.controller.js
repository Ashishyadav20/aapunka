(function () {
    'use strict';
    angular.module('angularProjects')
        .controller('loginController', loginController);
    /**@ngInject */
    function loginController(AppService, $rootScope) {
        var vm = this;
        activate();
        function activate() {
            vm.isContrast = JSON.parse(AppService.getLocalStorageItem('contrast')) != null ? JSON.parse(AppService.getLocalStorageItem('contrast')) : false;
            $rootScope.$on('isContrast', function (ev, data) {
                vm.isContrast = data;
            });
        }

    }
})()
(function () {
  'use strict';

  angular
    .module('angularProjects')
    .config(config);

  /** @ngInject */
  function config($logProvider, $locationProvider) {
    // Enable log
    $logProvider.debugEnabled(true);
    $locationProvider.hashPrefix('');

  }

})();

angular.module('angularProjects')
    .factory('appFactory', AppFactory);
/**@ngInject */
function AppFactory($cookies) {
    return {
        "getUserCookie": getUserCookie,
        'getOrderIdInCookie': getOrderIdInCookie
    }
    function getUniqueId() {
        var no = Math.floor(Math.random() * Math.floor(Math.random() * 20)) + 10000000;
        var timeStamp = Math.floor(Date.now());
        return no + timeStamp;
    }
    function getUserCookie() {
        if ($cookies.get('userId') == "" || $cookies.get('userId') == null) {
            var id = getUniqueId();
            $cookies.put('userId', id);
            return $cookies.get('userId');
        } else {
            return $cookies.get('userId');
        }
    }

    function getOrderIdInCookie(orderId) {
        if ($cookies.get('orderId') == "" || $cookies.get('orderId') == null) {
            $cookies.put('orderId', orderId);
            return $cookies.get('orderId');
        } else {
            return $cookies.get('orderId');
        }
    }
}

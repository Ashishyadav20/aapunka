(function () {
    'use strict';
    angular.module('angularProjects')
        .controller('ConfirmationController', ConfirmationController);
    /**@ngInject */
    function ConfirmationController($window, $state, $rootScope, AppService, $http) {
        var vm = this;
        activate();
        function activate() {
            // vm.isContrast = false;
            vm.sameShippingAddress = false;
            fetchStateList();
            vm.user = {};
            vm.paymentsMethods = getPaymentsMethods();
            vm.historyBack = historyBack;
            vm.placeOrder = placeOrder;
            vm.paymentMethodSelected = paymentMethodSelected;
            $rootScope.$on('isContrast', function (ev, data) {
                vm.isContrast = data;
            });
            vm.isContrast = JSON.parse(AppService.getLocalStorageItem('contrast')) != null ? JSON.parse(AppService.getLocalStorageItem('contrast')) : false;
        }

        function paymentMethodSelected(item) {
            vm.paymentType = item.name;
        }
        function getPaymentsMethods() {
            var jsonList = [{
                'id': 1,
                'name': 'Master card',
                'icon': 'assets/images/mastercard.png'
            },
            {
                'id': 2,
                'name': 'Visa card',
                'icon': 'assets/images/visa.png'
            }, {
                'id': 3,
                'name': 'Net Banking',
                'icon': 'assets/images/netBanking.png'
            }, {
                'id': 4,
                'name': 'Cash on Delivery',
                'icon': 'assets/images/cod.png'
            }];
            return jsonList;
        }
        function historyBack() {
            $window.history.back();
        }

        function fetchStateList() {
            $http.get('app/jsons/cities.json').then(function (stateResponse) {
                vm.stateList = stateResponse.data;
                vm.shippingStateList = angular.copy(vm.stateList);
            }, function () {
                vm.stateList = [];
            });
        }

        function placeOrder(status) {
            if (status && vm.selected) {
                var requestparams = setRequestparams();
                AppService.placeOrder(requestparams).then(function (success) {
                    console.log("success", success);
                    if ('success' in success.data) {
                        if (success.data.success) {
                            if ('order_id' in success.data) {
                                $state.go('orderPlaced', { orderId: success.data.order_id });
                            }

                        }
                    }

                }, function () {

                });

            } else {
                alert("Please check form again,something is missing!");
            }

        }

        function getDateFormatted() {
            var dt = new Date();
            var dateFormatted = dt.getFullYear() + '-' + (dt.getMonth() + 1) + '-' + dt.getDate();
            return dateFormatted;
        }

        function setRequestparams() {
            var request = {
                guest_id: AppService.AppFactory.getUserCookie(),
                order_date: getDateFormatted(),
                shipping_method: 'Heavy',
                payment_method: 1,
                order_status: 'pending',
                login_flag: false,
                shipping_address: getShippingAddress(),
                billing_address: getBillingAddress()
            }
            return request;
        }

        function getBillingAddress() {
            if (vm.sameShippingAddress) {
                return getShippingAddress();
            } else {
                return {
                    name: vm.user.shippingUserName,
                    address2: vm.user.shippingAddress,
                    address1: vm.user.shippingLocality,
                    city: vm.city,
                    state: vm.state,
                    pincode: vm.user.shippingPostalCode
                }
            }
        }

        function getShippingAddress() {
            return {
                name: vm.user.name,
                address2: vm.user.address,
                address1: vm.user.locality,
                city: vm.city,
                state: vm.state,
                pincode: vm.user.postalCode
            }
        }



    }
})()
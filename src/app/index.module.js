(function () {
  'use strict';

  angular
    .module('angularProjects', [
      'ngAnimate',
      'ui.router',
      'ngAria',
      'ngMaterial',
      'ksSwiper',
      'credit-cards',
      'ngCookies'
    ]);

})();

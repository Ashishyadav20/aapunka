(function () {
  'use strict';

  angular
    .module('angularProjects')
    .controller('ProductDetailsController', ProductDetailsController);

  /** @ngInject */
  function ProductDetailsController($rootScope, $state, AppService) {
    var vm = this;

    vm.appSetting = AppService.appSetting;
    vm.selectedProduct = AppService.getSelectedProduct();
    console.log("details", angular.toJson(vm.selectedProduct));
    activate();
    function activate() {
      vm.colorSetting = {
        'background': '#000',
        'color': '#fff'
      };
      $rootScope.$on('isContrast', function (ev, data) {
        vm.isContrast = data;
      });
      vm.isContrast = JSON.parse(AppService.getLocalStorageItem('contrast')) != null ? JSON.parse(AppService.getLocalStorageItem('contrast')) : false;
      vm.list = getList();
      vm.getSuggestedList = getSuggestedList();
      vm.reviewsList = getAudioReveiws();
      vm.videosImages = getVideosImages();
      vm.addToCart = addToCart;

    }
    function addToCart(item) {
      var jsonInput = {
        "guest_id": AppService.AppFactory.getUserCookie(),
        "quantity": item.quantity,
        "catalog": item.catalog,
        "isLogin": false
      };
      console.log("final add to cart", angular.toJson(jsonInput));
      AppService.addItemToCarts(jsonInput).then(function (success) {
        console.log("Success in addingto Cart", angular.toJson(success));
        $state.go('shoppingCart');
      }, function (error) {
        console.log("Error in addingto Cart", angular.toJson(error));
        $state.go('shoppingCart');
      })

    }

    function getList() {
      var jsonList = [{
        'id': 1,
        'name': 'Nab  Cane',
        'img': 'assets/images/nab_cane.jpg',
        'catalog': 'Wc001',
        'Price': '70',
        'quantity': 1,
        'descp': 'A white cane is a long rodlike device used by blind or visually impaired travelers to give them information about the environment they are traveling through. Using a cane can warn them of obstacles in their path, tell them of stairs they are coming to, warn them that they are coming up to a curb, and tell them of many other things in the environment that they must deal with. The cane will also do something else, it will alert others around them that they are blind, and this can be very helpful. Many, if not all, states have laws concerning how drivers must act when encountering a person using a white cane'
      }, {
        'id': 2,
        'name': 'NIVH Dehradun Cane',
        'img': 'assets/images/nivh_Dehradhun_cane.jpg',
        'catalog': 'Wc002',
        'Price': '72',
        'quantity': 1,
        'descp': 'A white cane is a long rodlike device used by blind or visually impaired travelers to give them information about the environment they are traveling through. Using a cane can warn them of obstacles in their path, tell them of stairs they are coming to, warn them that they are coming up to a curb, and tell them of many other things in the environment that they must deal with. The cane will also do something else, it will alert others around them that they are blind, and this can be very helpful. Many, if not all, states have laws concerning how drivers must act when encountering a person using a white cane'

      }, {
        'id': 3,
        'name': 'Worth Trust Vellore Cane',
        'img': 'assets/images/worth_trust_vellore_cane.jpg',
        'catalog': 'Wc003',
        'Price': '84',
        'quantity': 1,
        'descp': 'A white cane is a long rodlike device used by blind or visually impaired travelers to give them information about the environment they are traveling through. Using a cane can warn them of obstacles in their path, tell them of stairs they are coming to, warn them that they are coming up to a curb, and tell them of many other things in the environment that they must deal with. The cane will also do something else, it will alert others around them that they are blind, and this can be very helpful. Many, if not all, states have laws concerning how drivers must act when encountering a person using a white cane'

      }];
      return jsonList;
    }

    function getSuggestedList() {
      var jsonList = [{
        'id': 1,
        'name': 'Screen readers',
        'img': 'assets/images/screen_readers.jpg',
        'catalog': 'Wc001',
        'Price': '70',
        'quantity': 1,
        'descp': 'A white cane is a long rodlike device used by blind or visually impaired travelers to give them information about the environment they are traveling through. Using a cane can warn them of obstacles in their path, tell them of stairs they are coming to, warn them that they are coming up to a curb, and tell them of many other things in the environment that they must deal with. The cane will also do something else, it will alert others around them that they are blind, and this can be very helpful. Many, if not all, states have laws concerning how drivers must act when encountering a person using a white cane'
      }, {
        'id': 2,
        'name': 'Typings',
        'img': 'assets/images/typings.jpg',
        'catalog': 'Wc002',
        'Price': '72',
        'quantity': 1,
        'descp': 'A white cane is a long rodlike device used by blind or visually impaired travelers to give them information about the environment they are traveling through. Using a cane can warn them of obstacles in their path, tell them of stairs they are coming to, warn them that they are coming up to a curb, and tell them of many other things in the environment that they must deal with. The cane will also do something else, it will alert others around them that they are blind, and this can be very helpful. Many, if not all, states have laws concerning how drivers must act when encountering a person using a white cane'

      }, {
        'id': 3,
        'name': 'Watches',
        'img': 'assets/images/watches.jpg',
        'catalog': 'Wc003',
        'Price': '84',
        'quantity': 1,
        'descp': 'A white cane is a long rodlike device used by blind or visually impaired travelers to give them information about the environment they are traveling through. Using a cane can warn them of obstacles in their path, tell them of stairs they are coming to, warn them that they are coming up to a curb, and tell them of many other things in the environment that they must deal with. The cane will also do something else, it will alert others around them that they are blind, and this can be very helpful. Many, if not all, states have laws concerning how drivers must act when encountering a person using a white cane'

      }];
      return jsonList;
    }

    function getVideosImages() {
      var json = [{
        'id': 1,
        "icon": 'assets/images/video1.jpg'
      }, {
        'id': 2,
        "icon": 'assets/images/video2.jpg'
      }, {
        'id': 3,
        "icon": 'assets/images/video3.jpg'
      }, {
        'id': 4,
        "icon": 'assets/images/video4.jpg'
      }];
      return json;
    }


    function getAudioReveiws() {
      var jsonList = [{
        'id': 1,
        'name': 'Anthony1128',
        'review': 'This product rocks!',
        'img': 'assets/images/james.jpg'
      }, {
        'id': 2,
        'name': 'McCarthy_was_here',
        'review': 'My two cents',
        'img': 'assets/images/missandi.jpg'
      }, {
        'id': 3,
        'name': 'Sammmmy209',
        'review': 'just started using this product...',
        'img': 'assets/images/sammy.jpg'
      }];
      return jsonList;
    }
    $rootScope.$on('onSearch', function ($event, data) {
      vm.searchtext = data;
    })
  }
})();

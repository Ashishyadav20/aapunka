(function () {
  'use strict';

  angular
    .module('angularProjects')
    .directive('toolBar', acmeNavbar);

  /** @ngInject */
  function acmeNavbar() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directive/navbar/navbar.html',
      scope: {
      },
      controller: NavbarController,
      controllerAs: 'vm',
      bindToController: true
    };

    return directive;

    /** @ngInject */
    function NavbarController($rootScope, $state, $mdMenu, $mdToast, $mdSidenav,AppService) {
      var vm = this;
      vm.isContrast = false;
      vm.isContrast = JSON.parse(AppService.getLocalStorageItem('contrast')) != null ? JSON.parse(AppService.getLocalStorageItem('contrast')) : false;
      vm.searchTextChange = function (text) {
        $rootScope.$broadcast('onSearch', text);
      }
      vm.keepShoping = function () {
        $state.go('home', {}, { location: 'replace' })
      }
      vm.openShoppingCarts = function () {
        $state.go('shoppingCart');
      }
      vm.openMenu = function ($mdMenu, ev) {
        $mdMenu.open(ev);
      }
      vm.openHome = function () {
        $state.go('home', {}, { location: 'replace' });
      }
      vm.openCarts = function () {
        $state.go('shoppingCart');
      }
      vm.logout = function () {
        $mdToast.show(
          $mdToast.simple()
            .textContent('Comming soon!')
            .position("top")
            .hideDelay(1000)
        );
      }
      vm.openDrawer = function(){
        $mdSidenav('right').toggle();

      }

      $rootScope.$on('isContrast',function(ev,data){
        vm.isContrast = data;
      });
    }
  }

})();

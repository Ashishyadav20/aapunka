(function () {
    'use strict';
    angular.module('angularProjects')
        .directive('sideNav', sideNav);
    /**@ngInject */
    function sideNav() {
        return {
            restrict: 'E',
            scope: {
                events:'&'
            },
            controller: sideNavController,
            templateUrl: 'app/directive/sideNav/sideNav.html',
            controllerAs: 'vm',
            bindToController: true,
            link: link,

        }

        function sideNavController($rootScope,$mdSidenav) {
            var vm = this;
            vm.isContrast = false;
            $rootScope.$on('isContrast', function (ev, data) {
                vm.isContrast = data;
            });
        }
        function link(scope, attr, ele) {
        }
    }
})();
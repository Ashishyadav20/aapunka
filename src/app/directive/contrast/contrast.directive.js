(function () {
    'use strict';
    angular.module('angularProjects')
        .directive('contrast', contrast);
    /**@ngInject */
    function contrast() {
        return {
            restrict: 'E',
            template: '<div style="text-align: end;">' + '{{vm.name}}' +
            '<div ng-class="vm.isContrast?\'contrast contrastButton\':\'textGreyColor contrastButton\'"  ng-if="!vm.isContrast">' +
            '<md-button class="mediumFont" style="font-weight: bold;" ng-click="vm.contrast();">CLICK FOR HIGH CONTRAST</md-button>' +
            '</div>' +
            '<div ng-class="vm.isContrast?\'contrast contrastButton\':\'textGreyColor contrastButton\'"  ng-if="vm.isContrast">' +
            '<md-button style="font-weight: bold;" class="mediumFont" ng-click="vm.contrast();">CLICK FOR LOW CONTRAST</md-button>' +
            '</div>' +
            '</div>',
            controller: contrastController,
            scope: {},
            controllerAs: 'vm',

        }
        function contrastController($rootScope, AppService) {
            var vm = this;
            vm.isContrast = JSON.parse(AppService.getLocalStorageItem('contrast')) != null ? JSON.parse(AppService.getLocalStorageItem('contrast')) : false; vm.contrast = function () {
                vm.isContrast = !vm.isContrast;
                AppService.setLocalStorageItem('contrast', vm.isContrast);
                $rootScope.$emit('isContrast', vm.isContrast);
            }
        }

    }

})()
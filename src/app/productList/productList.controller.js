(function () {
  'use strict';

  angular
    .module('angularProjects')
    .controller('ProductListController', ProductListController);

  /** @ngInject */
  function ProductListController($rootScope, $stateParams, AppService, $state,sample) {
    var vm = this;
    console.log("Sample",sample);
    vm.showDom = false;
    vm.appSetting = AppService.appSetting;
    activate();
    function activate() {
      vm.colorSetting = {
        'background': '#000',
        'color': '#fff'
      };
      $rootScope.$on('isContrast', function (ev, data) {
        vm.isContrast = data;
      });
      vm.isContrast = JSON.parse(AppService.getLocalStorageItem('contrast')) != null ? JSON.parse(AppService.getLocalStorageItem('contrast')) : false;
      vm.productList = null;
      getProductsById($stateParams.id);
      vm.type = $stateParams.type;
      vm.openProductDetails = openProductDetails;

    }
    // alert(window.screen.availWidth);
    $(window).resize(function () {
      vm.windowWidth = $(window).width();
      console.log("old--" + vm.windowWidth);
    });

    vm.mouseEnter = function (index) {
      vm.onIndex = index;
    }
    function openProductDetails(selectedItem) {
      console.log("item", selectedItem);
      AppService.setSelectedProduct(selectedItem);
      $state.go('productDetails', { id: selectedItem.product_id, name: selectedItem.product_name });
    }
    function getProductsById(id) {
      AppService.getProductDetailsByID(id).then(function (success) {
        console.log("Products by id", angular.toJson(success));
        if (success.data.hasOwnProperty('result')) {
          vm.showDom = true;
          vm.productList = success.data.result;
        }
      }, function (error) {
        console.log("Products by id error", angular.toJson(error));
      });
    }
    function getProductList() {
      var jsonList = [{
        'id': 1,
        'name': 'Nab  Cane',
        'img': 'assets/images/nab_cane.jpg',
        'catalog': 'Wc001',
        'Price': '70',
        'quantity': 1,
        'descp': 'A white cane is a long rodlike device used by blind or visually impaired travelers to give them information about the environment they are traveling through. Using a cane can warn them of obstacles in their path, tell them of stairs they are coming to, warn them that they are coming up to a curb, and tell them of many other things in the environment that they must deal with. The cane will also do something else, it will alert others around them that they are blind, and this can be very helpful. Many, if not all, states have laws concerning how drivers must act when encountering a person using a white cane'
      }, {
        'id': 2,
        'name': 'NIVH Dehradun Cane',
        'img': 'assets/images/nivh_Dehradhun_cane.jpg',
        'catalog': 'Wc002',
        'Price': '72',
        'quantity': 1,
        'descp': 'A white cane is a long rodlike device used by blind or visually impaired travelers to give them information about the environment they are traveling through. Using a cane can warn them of obstacles in their path, tell them of stairs they are coming to, warn them that they are coming up to a curb, and tell them of many other things in the environment that they must deal with. The cane will also do something else, it will alert others around them that they are blind, and this can be very helpful. Many, if not all, states have laws concerning how drivers must act when encountering a person using a white cane'

      }, {
        'id': 3,
        'name': 'Worth Trust Vellore Cane',
        'img': 'assets/images/worth_trust_vellore_cane.jpg',
        'catalog': 'Wc003',
        'Price': '84',
        'quantity': 1,
        'descp': 'A white cane is a long rodlike device used by blind or visually impaired travelers to give them information about the environment they are traveling through. Using a cane can warn them of obstacles in their path, tell them of stairs they are coming to, warn them that they are coming up to a curb, and tell them of many other things in the environment that they must deal with. The cane will also do something else, it will alert others around them that they are blind, and this can be very helpful. Many, if not all, states have laws concerning how drivers must act when encountering a person using a white cane'

      }, {
        'id': 4,
        'name': 'Imported Cane',
        'img': 'assets/images/imported_cane.jpg',
        'catalog': 'Wc004',
        'Price': '90',
        'quantity': 1,
        'descp': 'A white cane is a long rodlike device used by blind or visually impaired travelers to give them information about the environment they are traveling through. Using a cane can warn them of obstacles in their path, tell them of stairs they are coming to, warn them that they are coming up to a curb, and tell them of many other things in the environment that they must deal with. The cane will also do something else, it will alert others around them that they are blind, and this can be very helpful. Many, if not all, states have laws concerning how drivers must act when encountering a person using a white cane'

      }];
      return jsonList;
    }
    $rootScope.$on('onSearch', function ($event, data) {
      vm.searchText = data;
    })
  }
})();

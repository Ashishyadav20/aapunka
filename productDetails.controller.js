(function () {
  'use strict';

  angular
    .module('angularProjects')
    .controller('ProductDetailsController', ProductDetailsController);

  /** @ngInject */
  function ProductDetailsController($rootScope,$stateParams,AppService) {
    var vm = this;
    vm.selectedProduct= AppService.getSelectedProduct
    alert($stateParams.type);
    activate();
    function activate() {
      vm.colorSetting = {
        'background': '#000',
        'color': '#fff'
      }
      vm.productList = getProductList();
    }
    function getProductList() {
      var jsonList = [{
        'id': 1,
        name: 'Nab  Cane',
        img: 'assets/images/canes.jpg',
        catalog: 'Wc001',
        Price: '70'
      }, {
        'id': 2,
        name: 'NIVH Dehradun Cane',
        img: 'assets/images/voice_recorders.jpg',
        catalog: 'Wc002',
        Price: '72'
      }, {
        'id': 3,
        name: 'Worth Trust Vellore Cane',
        img: 'assets/images/screen_readers.jpg',
        catalog: 'Wc003',
        Price: '84'
      }, {
        'id': 4,
        name: 'Imported Cane',
        img: 'assets/images/braille.jpg',
        catalog: 'Wc004',
        Price: '90'
      }];
      return jsonList;
    }
    $rootScope.$on('onSearch', function ($event, data) {
      vm.searchText = data;
    })
  }
})();
